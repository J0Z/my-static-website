import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <h1>Hi there</h1>
    <p>Welcome to your new Gatsby site.</p>
    <p>Now go and free Ukraine from ЗЕ.</p>
    <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
      <Image />
    </div>
    <Link to="/page-2/">Go back to page 2</Link>
      <div>
          Version: %%VERSION%%
      </div>
  </Layout>
)

export default IndexPage
